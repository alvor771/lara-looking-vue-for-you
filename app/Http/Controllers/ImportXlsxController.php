<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\Storage;

class ImportXlsxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function import($id = 0)
    {
        $xlsxPath = Config::get('xlsx.path');

        $collection = (new FastExcel)->import(Storage::path('public/'.$xlsxPath));
        $arrayCompiled = [];
        foreach ($collection as $value) {
            foreach ($value as $key => $title) {
                $exp_key = explode(':', $key);
                if ($exp_key[0] == 'Filter' AND !empty($title)) {
                    $arrayCompiled[$exp_key[1]][] = $title;
                }
            }
        }

        $indexedResults = $this->createAr($arrayCompiled);
        if (isset($indexedResults[$id])) {
            return response()->json($indexedResults[$id]);
        } else {
            return response()->json([
                'pdfPath' => Config::get('xlsx.pdfPath')
            ]);
        }
    }

    private function createAr($arr)
    {
        $newArr = [];
        $id = 0;
        foreach ($arr as $key => $item) {
            $newArr[$id]['select'] = $key;
            $newArr[$id]['options'] = $item;

            $id++;
        }

        return $newArr;
    }
}
